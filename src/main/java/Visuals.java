import java.io.FileNotFoundException;

/**
 *
 * @author Mohamed Mahmoud Mohamed Vall
 */
public class Visuals {

public char[] blank;/**The chosen word hidden as a bunch of blank "_"*/

public char[] getBlank() throws FileNotFoundException{
    
    WordPicker w = new WordPicker();
    String word = w.Word();
    try{
    
         blank = new char[word.length()];
    }catch(NullPointerException uhoh){
    
        System.out.println("We've got a problem Hudson...\n"
                + "NullPointerException thrown...");
    }
    
    for(int i=0;i<word.length();i++){
    
        if(word.charAt(i)=='\''){
        
            blank[i]='\'';
        }else if(word.charAt(i)=='-'){
        
            blank[i]='-';
        }else{
        
            blank[i]='_';
        }
    }
    return blank;
}/**<p>This method will, with the use of a loop, convert every character 
*of our word into "_", in order to hide the letters of our word*/

}