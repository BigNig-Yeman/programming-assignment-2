
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


/**
 *This program is a Hangman game program for Programming Assignment 2
 * @author Mohamed Mahmoud Mohamed Vall
 */

public class Hangman {
    
    public String PATH(){
    
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter the path of the english dictionary.");
        String PATH = sc.nextLine();
        return PATH;
    }/**Registers the typed path as a string that will be used in the
     getWordList method from the WordPicker class*/
   
    public static void main(String[]args) throws FileNotFoundException  {
                
        Scanner sc = new Scanner(System.in);

        WordPicker w = new WordPicker();
        Visuals v = new Visuals();
 

        
        double penalty = 1;
        double life = 9;
        
        String word = w.Word();/**the word the user has to find*/
        char[] blank = v.getBlank();/**said word hidden as a string of '_'*/
        
        ArrayList<Character> letters = new ArrayList<Character>();/**ArrayList 
         * that stores the letters that were already inputted*/
        
        
        System.out.println("Please choose a difficulty: 'easy', 'normal' or 'hard' ");
        String difficulty = sc.nextLine();  
        
        
        switch(difficulty){
            case "easy":
                
                penalty = 1;
                break;
                
            case"normal":
        
                penalty = 1.5;
                break;
                
        case"hard":
        
            penalty=3;
            break;
            
        default:
        
            System.out.println("Um...Please select a valid difficulty next time.");
            System.exit(0);
        }/**<p>Decided to add a difficulty system to this. Depending on the choice,
        *the penalty for making might be harsher or lighter. You'll see this later,
        *but every time the user guesses wrong, the program will take off points
        *from the "life" variable. The amount will be based on the difficulty.*/       
        
        while(life>0){
            
            
            System.out.println("Now, please try to guess the hidden word.");
            System.out.println(blank);
            
            char l =sc.next().charAt(0);
            //The program will get a letter from input and store it in the char l
            
            letters.add(l);
            
            if(word.contains(l+"")){
                //Checks if the inputed letter is in the word
                
                for (int i=0;i<word.length();i++){
                    //This loop will check each specific letter to find which one matches
                    
                    if(word.charAt(i)==l){
                        
                        blank[i]=l;
                        /*Once the loops find the letter, it replaces it's matching
                        _ by the matching letter*/
                    }
                }
            }else{
                
                life = life-penalty;
            }//If the inputed letter isn't found, the user gets a penalty
            if (word.equalsIgnoreCase(String.valueOf(blank))){
            
                System.out.println(blank);
                System.out.println("Congratulations! You did it!! \n"
                        + "YOU WON A GAME OF HANGMAN!");
                
                break;
            }
            /*And if the contents of the blank char(in other words, all your
            letters) match the letter of the word, then you win*/
            
            else if(life==0){
        
            System.out.println("Sorry mate, but it's GAME OVER YEAAH");
            System.out.println("The word was '"+word+"'...Yeah, that's a word.");
            
            break;}
            //And if you lose... the program lets you know that.
        
        String body = "";    
            
        if(life <= 9 && life > 8){
        
                    body =    "+---+\n" +
                                "      |\n" +
                                "      |\n" +
                                "      |\n" +
                                "      |\n" +
                                "      |\n" +
                                "=========";
        
    }else if(life <= 8 && life > 7){
     
                    body= "+---+\n" +
                            "  |   |\n" +
                            "      |\n" +
                            "      |\n" +
                            "      |\n" +
                            "      |\n" +
                            "=========";
        
    }else if (life <= 7 && life > 6){
     
                    body= " +---+\n" +
                            "  |   |\n" +
                            "  O   |\n" +
                            "      |\n" +
                            "      |\n" +
                            "      |\n" +
                            "=========";
        
    }else if(life <= 6 && life > 5){
        
                    body= "+---+\n" +
                            "  |   |\n" +
                            "  O   |\n" +
                            "  |   |\n" +
                            "      |\n" +
                            "      |\n" +
                            "=========";
    }else if(life <= 5 && life > 4){
    
                    body= " +---+\n" +
                            "  |   |\n" +
                            "  O   |\n" +
                            " /|   |\n" +
                            "      |\n" +
                            "      |\n" +
                            "=========";
    }else if (life <=4 && life > 3){
    
                    body= "+---+\n" +
                            "  |   |\n" +
                            "  O   |\n" +
                            " /|\\  |\n" +
                            "      |\n" +
                            "      |\n" +
                            "=========";
    }else if (life <= 3 && life > 2){
    
                    body= "+---+\n" +
                            "  |   |\n" +
                            "  O   |\n" +
                            " /|\\  |\n" +
                            "   |  |\n" +
                            "      |\n" +
                            "=========";
    }else if (life <= 2 && life > 1){
    
                    body= "+---+\n" +
                            "  |   |\n" +
                            "  O   |\n" +
                            " /|\\ |\n" +
                            "  |   |\n "+                
                            "/    |\n" +
                            "=========";
    }else if(life<= 1){
    
                    body= " +---+\n" +
                            "  |   |\n" +
                            "  O   |\n" +
                            " /|\\  |\n" +
                            "  |   |\n"+
                            "/ \\ |\n" +
                            "=========";
    }else{
                    body= " +---+\n" +
                            "  |   |\n" +
                            "      |\n" +
                            "      |\n" +
                            "      |\n" +
                            "      |\n" +
                            "=========";
    }
            
                System.out.println(body);
                System.out.println("You have "+ life +" points remaining");
                //Visual representation of the user's progress and remaining life
        
        }
        
    }
}
