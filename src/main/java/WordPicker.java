
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mohamed Mahmoud Mohamed Vall
 */
public class WordPicker{

    public ArrayList getWordList() throws FileNotFoundException{
        
        Hangman H = new Hangman();
        File dictionary = new File(H.PATH());/*takes the PATH string from the 
        main class and uses it as the path of the file*/
        
        Scanner scDictionary = new Scanner(dictionary);
        /**<p>Turned the dictionary file into a java variable so that we can use it in 
        *in an upcoming method. Also created a scanner to scan the file later on.*/
        
        ArrayList wordList = new ArrayList<String>();
        while (scDictionary.hasNextLine()){
        
            wordList.add(scDictionary.nextLine());
            
        }
        scDictionary.close();
        return wordList;
    }/**<p>This method will allow us to turn the entire dictionary into an ArrayList
        *by scanning every line and adding it to the ArrayList "wordList".*/ 

    public String Word() throws FileNotFoundException {
        
        WordPicker w = new WordPicker();
        Random randomness = new Random();
        String word = "";
        word  =(String) w.getWordList().get(randomness.nextInt(w.getWordList().size()));
        return word;
    }/**<p>By initializing a random value, we can use the .get method to pick a  
    *random element from the ArrayList by placing that random number as the
    *index of the word we would pick. Now the word picked by this method
    *will be random.*/
}
